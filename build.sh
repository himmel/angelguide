#!/bin/sh

if [ $# -ne 1 ]; then
	echo expected revision as option >&2
	exit 1
fi

revision="$1"
version=$(echo $revision | grep -Eo 'v[0-9.]*')
commit_date=$(git show -s --format=%ci ${revision} | cut -c1-10)

if [ -z "${version}" ]; then
  echo building with revision ${revision}
  asciidoctor-pdf -a revnumber=${revision} -a revdate=${commit_date} -o angelguide_en.pdf guide.adoc
  asciidoctor-pdf -a revnumber=${revision} -a revdate=${commit_date} -o angelguide_de.pdf -a lang=de guide.adoc
else
  echo building with version ${version}
  asciidoctor-pdf -a revnumber=${version} -o angelguide_en.pdf guide.adoc
  asciidoctor-pdf -a revnumber=${version} -o angelguide_de.pdf -a lang=de guide.adoc
fi
