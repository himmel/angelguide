== icon:hands-helping[size=fw]  Was ist ein Engel?

NOTE: Jeder, der seine Freizeit in unsere Veranstaltung investiert, ist ein Engel. +

Während die offensichtlichsten Engel für die meisten Teilnehmer die Helfer sind, die den technischen Support besetzen, die Streams produzieren oder Trolle in den öffentlichen Channeln im Zaum halten, gibt es eine Vielzahl von Aufgaben, die "hinter den Kulissen" und dabei kaum wahrnehmbar zu erledigen sind (oder erst dann sichtbar werden, wenn etwas schief geht).

All diese Menschen, von einer neuen Person, die das erste Mal auf einem Chaos-Event mithilft, bis hin zu den erfahrenen Leuten im Organisationsteam, die in den Monaten und Wochen vor der Veranstaltung die anspruchsvolle Planung durchführen - sie alle sind Engel.

=== Vorteile

Ein Engel zu sein, bringt auch einige Vorteile mit sich. Obwohl wir hoffen, dass die Teilnahme Belohnung genug ist, gibt es hier eine Liste von Dingen, die exklusiv für Engel sind.

* Abhängen im Himmel und dem Engel-Hackcenter mit seinem Chill-Out-Bereich.
* Spezielle Engel-Badges in der rC3-World
* Anerkennung der Community

=== Belohnungen

Wenn du eine gewisse Zeit beigetragen hast, erhältst du ggf. weitere Goodies.

[IMPORTANT]
.Ressourcenbeschränkung
====
Bitte beachte, dass unsere Ressourcen begrenzt sind. Vielleicht bekommt nicht jeder Helfer weitere Goodies.
====

=== Erwartungen

IMPORTANT: Die Hilfe bei unseren Veranstaltungen bringt auch einige einfache, aber wichtige Erwartungen an dich mit sich. +

* Sei pünktlich zu deiner Schicht oder informiere den Himmel frühzeitig.
* Sei gut ausgeruht und nüchtern.
* Habe funktionierende Technik (Internet, Headset, ...)
* Sei aufgeschlossen und freundlich eingestellt.
* Lebe unsere moralischen Werte:
** Seid freundlich zueinander.
** Alle Kreaturen sind willkommen.

=== Eingeschränkte Engeltypen

Wie du im Engelsystem vielleicht schon gesehen hast, gibt es ein paar Engeltypen, denen du einfach so beitreten kannst und einige Engeltypen, die eingeschränkt sind. Normalerweise wird jede Aufgabe, die nicht viel Einführung benötigt, mit dem offenen "Angel"-Engeltyp versehen. Alle anderen Aufgaben benötigen irgendeine Form der Einführung wie in verwendete Hardware oder andere Voraussetzungen, wie z.B. bestimmte Nachweise über Fachkenntnisse. Rettungssanitäter, Feuerwehrmänner oder Personen mit Hygienezertifikaten müssen unter Umständen als solche bestätigt werden.

Üblicherweise stehen die Voraussetzungen für eine Aufnahme in einen Engeltypen in der Beschreibung des Engeltypen. Typischerweise ist dies meistens der Besuch einer Einführungsveranstaltung, in der notwendinges Fachwissen vermittelt wird oder die Voraussetzungen für den Typ überprüft werden. Diese Einführungsveranstaltungen werden im Engelsystem unter dem Reiter "Treffen" angekündigt. Hat man erfolgreich an einer Einführungsveranstaltung teilgenommen, wird man für den jeweiligen Engeltypen freigeschaltet und kann sich anschließend in passende Schichten eintragen.

Falls darüber hinaus noch Fragen auftauchen, stehen in der Beschreibung des Engeltypen auch meistens Kontaktdaten wie eine DECT-Nummer oder eine E-Mail-Adresse, die genutzt werden können. Alternativ kann auch eine der unter "Supporter" hinterlegten Personen des jeweiligen Engeltypen gefragt werden.
