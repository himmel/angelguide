== icon:users[size=fw] Teams

Die rC3 wird von verschiedenen Teams organisiert, die jeweils ein eigenes Fachgebiet abdecken.

Alle Teams sind selbst organisiert und bieten der rC3 ihre eigenen Dienstleistungen an. Um dir einen Überblick zu geben, beschreiben wir hier die Teams, mit denen du am ehesten interagieren wirst und geben einen kurzen Überblick über deren Zusammenspiel.

NOTE: Teams entstehen durch ein unerfülltes Bedürfnis. Sie werden selten von einer Autorität ins Leben gerufen.

<<<
include::teams/awareness-team.adoc[]

//include::teams/boc.adoc[]

//include::teams/bottles-team.adoc[]

include::teams/cashdesk.adoc[]

include::teams/cert.adoc[]

include::teams/c3gelb.adoc[]

//include::teams/hac.adoc[]

include::teams/heaven.adoc[]

include::teams/infodesk.adoc[]

//include::teams/loc.adoc[]

// include::teams/noc.adoc[]

include::teams/pl.adoc[]

include::teams/poc.adoc[]

//include::teams/security.adoc[]

include::teams/voc.adoc[]
