=== PL [small]#(Project Lead / Projektleitung)#

Die PL ist der Chaos - Real World Adapter.
Sie sind für die rC3 verantwortlich und ermöglichen es uns, all die lustigen Dinge zu machen, die wir mögen.

Der Umgang mit realer deutscher Bürokratie und die Kommunikation zwischen den Teams ist ihre Kompetenz.
