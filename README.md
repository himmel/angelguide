# Angel Guide for Chaos Events

This guide is written in Asciidoc.

## Requirements

* asciidoctor
* asciidoctor-pdf

## Asciidoctor resources

* [Quick Reference](https://asciidoctor.org/docs/asciidoc-syntax-quick-reference/)
* [User Manual](https://asciidoctor.org/docs/user-manual/)

## How to build the PDF

Run the following command in the git repositories root folder, after installing
the requirements.

```
  $ asciidoctor-pdf guide.adoc
```

Build the guide in other languages:

```
  $ asciidoctor-pdf -a lang=de guide.adoc
```

## Deployment via CI

The guide can be built and deployed to our web server using Gitlab CI. The
target server, user and path as well as the SSH key used for deployment are
configured in the settings of this project and the server can be prepared
using the `angelguide-deployment` role from our
[infrastructure repo](https://chaos.expert/himmel/infrastruktur).

`PUBLISH_PATH` is always `/`. `PUBLISH_HOST`, `PUBLISH_USER` and
`SSH_PRIVATE_KEY` must match the values configured via Ansible.

While the build process of the PDFs is happening automatically for every commit
and merge request, the deployment job is a manual action. After the deployment,
the guide will be reachable at:

* <https://c3heaven.de/guide/angelguide_de.pdf> (german)
* <https://c3heaven.de/guide/angelguide_en.pdf> (english)
* <https://c3heaven.de/angelguide.pdf> (language determined by browser)
